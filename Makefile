NAME=sal

# must be empty for english
#TRANSLATE=_fr

ROOMS:= start start_town start_underground start_labyrinth\
	town beach bateau forest

SONGS:= start 

GAME_C_FILES = main.c \
	lib/blitter/blitter.c \
	lib/blitter/blitter_tmap.c \
	lib/blitter/blitter_sprites.c \
	lib/mod/mod32.c\
	$(ROOMS:%=room_%$(TRANSLATE).c) \
	object.c \
	player.c \
	resources.c \
	behaviours.c \
	behaviours.c \
	window.c 

DEFINES = VGA_MODE=320 VGA_BPP=8 

# will be used in data.mk
TMX = python2 $(BITBOX)/lib/blitter/scripts/tmx2.py -mo data
TSX = python2 $(BITBOX)/lib/blitter/scripts/tsx.py  -mo data

include $(BITBOX)/kernel/bitbox.mk 

# generates all dependencies of data
.DELETE_ON_ERROR: build/data.mk
build/data.mk: $(ROOMS:%=rooms/%.tmx) window.tmx
	@mkdir -p build
	@mkdir -p data
	$(BITBOX)/lib/blitter/scripts/tmx-deps.py $^ > $@
-include build/data.mk

main.c: data.h # should be automatic

TOCOMPRESS :=  room_dark.tset tiles_town.tset tiles_bateau.tset tiles_beach.tset town_nuit.tset tiles_cavern.tset\
	bed.spr boss.spr bonh.spr rat.spr \
	$(ROOMS:%=%.tmap)
	
RAWFILES:= $(filter-out $(TOCOMPRESS:%=data/%),$(TMX_DATAFILES)) $(SONGS:%=sound/%.mod)

data/%.lz4: data/%
	lz4 -f -9 --content-size --no-frame-crc --no-sparse $^ $@
	

data.h : $(TOCOMPRESS:%=data/%.lz4) $(RAWFILES)
	# uurgh. this just appends standard names but replace cannot replace twice .. 
	python $(BITBOX)/lib/resources/embed.py $(foreach fn,$(TOCOMPRESS),data/$(fn).lz4:$(subst .,_,$(fn))) $(RAWFILES) > data.h

clean::
	rm -rf $(ROOMS:%=room_%.h) data data.h sprite_*.h map_*.h room_*_fr.c

%_fr.c: %.c trans_fr.po
	./i18n.py $< fr
